# Capybara Cheat Sheet

##Navigating

```sh
visit('/')
visit root_path
```


##Clicking links and buttons

```sh
click_link(['id-of-link', 'link text'])
click_button(['id-of-button', 'name-of-button', 'button text'])
click_on(['[link, button] text', 'id-of-[link, button]'])
```


##Interating with forms

```sh
fill_in(['label text', 'id-of-input', 'name-of-input'], :with => 'John')
choose('A radio button')
check('A checkbox')
uncheck('A checkbox')
attach_file('Image', '/path/to/image.jpg')
select('Option', :from => 'Select box')
```


##Scoping

```sh
within("//li[@id='employee']") do
  fill_in 'Name', :width => 'John'
end
within("li#employee") do
  fill_in 'Name', :width => 'John'
end
within_fieldset('[id, legend] of fieldset') do
  fill_in 'Name', :width => 'John'
end
within_table('[id, caption] of the table') do
  fill_in 'Name', :width => 'John'
end
```


##Querying

```sh
page.has_xpath?('//table/tr')
page.has_css?('table tr.foo')
page.has_content?('foo')
page.should have_xpath()
page.should have_css()
page.should have_content()
page.should have_no_content()
find_field('').visible?
find_link().visible?
find_button().click
find().click
all('a').each { |a| a[:href] }
```


##Srcipting

```sh
result = page.evaluate_script('4 + 4');
```

##XPath and CSS

```sh
within(:css, 'ul li') { ... }
find(:css, 'ul li#id-of-list').text
locate(:css, 'input#name').value
within('ul li') { ... }
find('ul li#id-of-list').text
locate('input#name').value
```